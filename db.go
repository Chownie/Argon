package main

import (
	"database/sql"
	"fmt"
	"strconv"

	"github.com/bwmarrin/discordgo"
	_ "github.com/mattn/go-sqlite3"
)

var db *sql.DB

func getDBConnection() *sql.DB {
	database, err := sql.Open("sqlite3", "./modlog.sqlite")

	if err != nil {
		panic(err)
	}
	return database
}

func checkError(err error) bool {
	if err != nil {
		fmt.Println(err)
		return true
	}
	return false
}

func logBan(user *discordgo.User, reason string) {
	if user == nil || reason == "" {
		fmt.Println("User: ", user)
	}

	userExists(user)
	tx, err := db.Begin()
	checkError(err)

	stmt, err := tx.Prepare("insert into bans(userid, reason) values(?, ?)")
	checkError(err)

	uid, err := strconv.Atoi(user.ID)
	checkError(err)

	stmt.Exec(uid, reason)
	tx.Commit()
	stmt.Close()
}

func logMessage(message *discordgo.Message) {
	if message == nil {
		fmt.Println("Message: ", message)
		return
	}
	userExists(message.Author)

	tx, err := db.Begin()
	checkError(err)

	stmt, err := tx.Prepare("insert into messages(messageid, userid, channel, content, attachment, timestamp) values(?, ?, ?, ?, ?, ?)")
	checkError(err)

	attachments := ""
	for _, v := range message.Attachments {
		attachments += v.URL + "\n"
	}

	stmt.Exec(message.ID, message.Author.ID, message.ChannelID, message.ContentWithMentionsReplaced(), attachments, message.Timestamp)
	tx.Commit()
	stmt.Close()
}

func logDeletion(message *discordgo.Message) {
	if message == nil {
		fmt.Println("Message: ", message)
		return
	}

	tx, err := db.Begin()
	checkError(err)

	stmt, err := tx.Prepare("insert into deletions(messageid) values(?)")
	checkError(err)

	res, err := stmt.Exec(message.ID)
	tx.Commit()
	stmt.Close()

	checkError(err)
	fmt.Println(res)
}

func createUser(user *discordgo.User) {
	if user == nil {
		fmt.Println("User: ", user)
		return
	}

	tx, err := db.Begin()
	checkError(err)

	stmt, err := tx.Prepare("insert into users(userid, nick) values(?, ?)")
	checkError(err)

	res, err := stmt.Exec(user.ID, user.Username)
	checkError(err)

	err = tx.Commit()
	stmt.Close()
	checkError(err)

	rowsAffected, err := res.RowsAffected()
	checkError(err)
	insertID, err := res.LastInsertId()
	checkError(err)

	fmt.Println("Result: ", insertID, " | ", rowsAffected)
}

func userExists(user *discordgo.User) bool {
	if user == nil {
		fmt.Println("User: ", user)
		return false
	}
	fmt.Println("Checking a user's existance")

	tx, err := db.Begin()
	checkError(err)

	stmt, err := tx.Prepare("select count(userid) from users where userid = ?")
	checkError(err)
	count := 0
	err = stmt.QueryRow(user.ID).Scan(&count)
	tx.Commit()
	stmt.Close()
	checkError(err)

	fmt.Println("Users with the ID ", user.ID, " = ", count)

	if count == 1 {
		fmt.Println("user exists")
		return true
	}
	fmt.Println("user being created")
	createUser(user)
	return false
}
