package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
)

var lastRequest map[string]userTimeoutScore

// Variables used for command line parameters
var (
	Token string
	BotID string
)

type userTimeoutScore struct {
	LastCommanded time.Time
	Score         int
}

func sendPM(st *discordgo.Session, userid string, message string) {
	channel, err := st.UserChannelCreate(userid)
	checkError(err)
	st.ChannelMessageSend(channel.ID, message)
}

func canUseCommand(st *discordgo.Session, id string, usingCommand bool) bool {
	gm, err := st.GuildMember("215514624664141824", id)
	checkError(err)

	guildRoles, err := st.GuildRoles("215514624664141824")
	checkError(err)

	var val userTimeoutScore
	var ok bool

	if val, ok = lastRequest[id]; !ok {
		lastRequest[id] = userTimeoutScore{time.Now(), 0}
		return true
	}

	if usingCommand {
		for _, v := range guildRoles {
			for _, k := range gm.Roles {
				if k == v.ID && v.Name == "Naughty Boys" {
					sendPM(st, id, "You're on the naughty boy's list! No bot commands for you")
					return false
				}
			}
		}
		if time.Since(lastRequest[id].LastCommanded) < (time.Second * 15) {
			if val.Score >= 4 {
				sendPM(st, id, "You've been doing that much. Try to make fewer than ~4 requests per minute.")
				return false
			}
			val.Score++
			val.LastCommanded = time.Now()
			lastRequest[id] = val
			return true
		}
	}
	if time.Since(val.LastCommanded) > (time.Second * 5) {
		if val.Score > 0 {
			val.Score--
		}
		lastRequest[id] = val
		return true
	}
	return true
}

type jsonFormat struct {
	FileURL string `json:"file_url"`
}

func init() {
	flag.StringVar(&Token, "t", "", "Bot Token")
	flag.Parse()

	db = getDBConnection()
}

func main() {
	defer db.Close()
	lastRequest = make(map[string]userTimeoutScore)
	// Create a new Discord session using the provided bot token.
	dg, err := discordgo.New("Bot " + Token)
	if err != nil {
		fmt.Println("error creating Discord session,", err)
		return
	}

	// Get the account information.
	u, err := dg.User("@me")
	if err != nil {
		fmt.Println("error obtaining account details,", err)
	}

	// Store the account ID for later use.
	BotID = u.ID

	// Register messageCreate as a callback for the messageCreate events.
	dg.AddHandler(messageCreate)
	dg.AddHandler(messageDelete)
	//dg.AddHandler(userBan)

	// Open the websocket and begin listening.
	err = dg.Open()
	if err != nil {
		fmt.Println("error opening connection,", err)
		return
	}

	fmt.Println("Bot is now running.  Press CTRL-C to exit.")
	// Simple way to keep program running until CTRL-C is pressed.
	<-make(chan struct{})
	return
}

func e621Search(args []string) string {
	tags := strings.Join(args, "+")
	resp, err := http.Get("https://e621.net/post/index.json?limit=30&tags=" + tags)
	if err != nil {
		return err.Error()
	}

	defer resp.Body.Close()
	jsonText, err := ioutil.ReadAll(resp.Body)
	checkError(err)

	rand.Seed(time.Now().Unix())

	var responses []jsonFormat
	err = json.Unmarshal(jsonText, &responses)
	checkError(err)
	var item jsonFormat
	if len(responses) > 1 {
		item = responses[rand.Intn(len(responses)-1)]
	} else if len(responses) == 1 {
		item = responses[0]
	} else {
		return "Nothing for that tag combination found."
	}
	return item.FileURL
}

func parseMessage(s *discordgo.Session, m *discordgo.MessageCreate) {
	tokens := strings.Fields(m.Content)
	if len(tokens) < 1 {
		return
	}

	switch tokens[0] {
	case "~furry":
		if canUseCommand(s, m.Author.ID, true) {
			s.ChannelMessageSend(m.ChannelID, e621Search(tokens[1:]))
		}
	case "~score":
		if canUseCommand(s, m.Author.ID, true) {
			s.ChannelMessageSend(m.ChannelID, strconv.Itoa(lastRequest[m.Author.ID].Score))
		}
	default:
		canUseCommand(s, m.Author.ID, false)
	}
}

func messageDelete(s *discordgo.Session, m *discordgo.MessageDelete) {
	logDeletion(m.Message)
}

func userBan(s *discordgo.Session, u *discordgo.GuildBan) {
	logBan(u.User, u.Reason)
}

// This function will be called (due to AddHandler above) every time a new
// message is created on any channel that the autenticated bot has access to.
func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	// Ignore all messages created by the bot itself
	if m.Author.ID == BotID {
		return
	}

	if strings.HasPrefix(m.Content, "~") {
		parseMessage(s, m)
	} else {
		canUseCommand(s, m.Author.ID, false)
	}

	logMessage(m.Message)

	// If the message is "ping" reply with "Pong!"
	if m.Content == "ping" {
		_, _ = s.ChannelMessageSend(m.ChannelID, "Pong!")
	}
}
